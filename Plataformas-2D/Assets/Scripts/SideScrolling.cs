using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideScrolling : MonoBehaviour
{
    private Transform player;

    private void Awake()
    {
        player = GameObject.FindWithTag("Player").transform;
    }

    private void LateUpdate()
    {
        Vector3 cameraPosition = transform.position;
        if (player.position.y > 0)
        {
            cameraPosition.y = player.position.y;
        }
        cameraPosition.x = player.position.x;
        transform.position = cameraPosition;
    }
}
