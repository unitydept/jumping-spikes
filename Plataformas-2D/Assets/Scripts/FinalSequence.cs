using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalSequence : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Transform door;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
                GameManager.Instance.isTimeRunning = false;
                DisableMovement();
                AudioManager.Instance.PauseMusic();
                StartCoroutine(CompleteLevelAnimation(player.transform, door.position));
        }
    }

    private IEnumerator CompleteLevelAnimation(Transform player, Vector3 destination)
    {
        float speed = 3f;
        Scene scene = SceneManager.GetActiveScene();
        player.GetComponent<Animator>().SetBool("isRunning", true);
        while (Vector3.Distance(player.position, destination) > 0.125f)
        {
            player.position = Vector3.MoveTowards(player.position, destination, speed * Time.deltaTime);
            yield return null;
        }
        player.GetComponent<Animator>().SetBool("isRunning", false);
        yield return new WaitForSeconds(1f);
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        if (scene.name == "Level-1")
        {
            GameManager.Instance.CompleteLevel1();
        }
        else
        {
            GameManager.Instance.CompleteLevel2();
        }
    }

    private void DisableMovement()
    {
        player.GetComponent<PlayerController>().enabled = false;
    }

}
