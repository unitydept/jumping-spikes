using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour
{
    public float speed = 1f;
    public Vector2 direction = Vector2.left;

    private Rigidbody2D myRigidbody;
    private Vector2 velocity;
    private bool isFacingRight = false;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        enabled = false;
    }

    private void OnBecameVisible()
    {
        enabled = true;
    }

    private void OnBecameInvisible()
    {
        enabled = false;
    }

    private void OnEnable()
    {
        myRigidbody.WakeUp();
    }

    private void OnDisable()
    {
        myRigidbody.velocity = Vector2.zero;
        myRigidbody.Sleep();
    }

    private void FixedUpdate()
    {
        velocity.x = direction.x * speed;
        velocity.y += Physics2D.gravity.y * Time.fixedDeltaTime;

        myRigidbody.MovePosition(myRigidbody.position + velocity * Time.fixedDeltaTime);

        //if (myRigidbody.Raycast(direction))
        //{
        //    direction = -direction;
        //    Flip();
        //}

        //if (myRigidbody.Raycast(Vector2.down))
        //{
        //    velocity.y = Mathf.Max(velocity.y, 0f);
        //}

    }
    //look right or left
    private void Flip()
    {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
    }
}
