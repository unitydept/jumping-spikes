using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnimation : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite deadSprite;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Reset()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        UpdatedSprite();
        DisablePhysics();
        StartCoroutine(Animate());
    }

    //Change to dead sprite and place player sprite above all tiles
    private void UpdatedSprite()
    {
        spriteRenderer.enabled = true;
        spriteRenderer.sortingOrder = 10;

        if (deadSprite != null)
        {
            spriteRenderer.sprite = deadSprite;
        }
    }

    //Disable physics to make player fall through the screen
    private void DisablePhysics()
    {
        Collider2D[] colliders = GetComponents<Collider2D>();

        foreach (Collider2D collider in colliders)
        {
            collider.enabled = false;
        }

        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;

        PlayerController playerController = GetComponent<PlayerController>();
        //SimpleMovement simpleMovement = GetComponent<SimpleMovement>();
        GameManager.Instance.isTimeRunning = false;

        if (playerController != null)
        {
            playerController.enabled = false;
        }

        //if (simpleMovement != null)
        //{
        //    simpleMovement.enabled = false;
        //}
    }

    //jump animation after death
    private IEnumerator Animate()
    {
        float elapsed = 0f;
        float duration = 3f;

        float jumpPower = 5f;
        float gravity = -7f;
        rb.velocity = new Vector2(rb.velocity.x, 0f);

        Vector3 velocity = Vector3.up * jumpPower;

        while (elapsed < duration)
        {
            transform.position += velocity * Time.deltaTime;
            velocity.y += gravity * Time.deltaTime;
            elapsed += Time.deltaTime;
            yield return null;
        }
    }
}
