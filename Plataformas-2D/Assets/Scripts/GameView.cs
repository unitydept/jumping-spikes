using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameView : MonoBehaviour
{
    public static GameView Instance;

    [SerializeField] private TextMeshProUGUI TimeValue;

    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
            TimeValue.text = GameManager.Instance.time.ToString("f0");
    }
}
