using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    [SerializeField] private float timeToDestroy;
    void Update()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
