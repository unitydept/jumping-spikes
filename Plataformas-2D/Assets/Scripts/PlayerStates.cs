using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour
{
    [SerializeField] private Animator smallAnimator;
    [SerializeField] private DeathAnimation deathAnimation;
    public bool small => smallAnimator.enabled;

    //Called when player touches spikes and laser trap to kill player nad reset level
    public void Death()
    {
        smallAnimator.enabled = false;
        deathAnimation.enabled = true;

        AudioManager.Instance.PauseMusic();
        GameManager.Instance.ResetLevel(3f);
    }
}
