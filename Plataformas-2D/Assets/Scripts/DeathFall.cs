using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFall : MonoBehaviour
{
    [SerializeField] private PlayerStates playerStates;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Fall);
            playerStates.Death();
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
