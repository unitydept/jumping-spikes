using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 8f;
    [SerializeField] private float jumpingPower = 16f;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private LayerMask groundLayer;

    [SerializeField] private Animator playerAnimator;

    [SerializeField] private Transform wallCheck;
    [SerializeField] private LayerMask wallLayer;

    private Camera myCamera;

    private Collider2D playerCollider;
    private float horizontal;

    public bool isWallSliding;


    private void Awake()
    {
        myCamera = Camera.main;
        playerCollider = GetComponent<Collider2D>();
    }
    private void OnEnable()
    {
        rb.isKinematic = false;
        playerCollider.enabled = true;
        rb.velocity = Vector2.zero;
    }
    private void OnDisable()
    {
        rb.isKinematic = true;
        playerCollider.enabled = false;
        rb.velocity = Vector2.zero;
    }

    private void Update()
    {
            horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && (IsGrounded() || isWallSliding))
        {
            if (isWallSliding)
            {
                rb.AddForce(new Vector2(-600 * gameObject.transform.localScale.x, 0));
            }

            isWallSliding = false;
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
            AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Jump);
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        playerAnimator.SetBool("isGrounded", IsGrounded());
        playerAnimator.SetFloat("velocityY",rb.velocity.y);

        playerAnimator.SetBool("isRunning", Mathf.Abs(rb.velocity.x) >= 1f);
        playerAnimator.SetBool("isWallSliding", isWallSliding);

        if (isWallSliding && !IsGrounded())
        {
            WallSlide();
        }
        else
        {
            rb.gravityScale = 3;
        }
    }
    private void FixedUpdate()
    {

        float targetSpeed = horizontal * speed;
        float speedDifference = targetSpeed - rb.velocity.x;
        float accelerationRate = (Mathf.Abs(targetSpeed) > 0.1f) ? 3.5f : 3.5f;
        float movement = Mathf.Pow(Mathf.Abs(speedDifference) * accelerationRate, 1) * Mathf.Sign(speedDifference);

        rb.AddForce(movement * Vector2.right);
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -12, 12), rb.velocity.y);

        if (Mathf.Abs(rb.velocity.x) >= 0.01f)
        {
            gameObject.transform.localScale = new Vector2(Mathf.Sign(rb.velocity.x), 1);
        }
    }

    //Check if character is on the ground, returns true if it is and false if not
    private bool IsGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.CapsuleCast(this.GetComponent<CapsuleCollider2D>().bounds.center, new Vector2(this.GetComponent<CapsuleCollider2D>().bounds.size.x -0.3f,
                                                        this.GetComponent<CapsuleCollider2D>().bounds.size.y), CapsuleDirection2D.Vertical, 0f, Vector2.down, 0.1f, groundLayer);
        return raycastHit.collider != null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //If player touches spikes play spike sound and kill player
        if (collision.gameObject.layer == LayerMask.NameToLayer("Spikes"))
        {
            PlayerStates player = gameObject.GetComponent<PlayerStates>();

            rb.velocity = new Vector2(rb.velocity.x, jumpingPower / 2.5f);
            AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Spikes);
            player.Death();
        }
        //if player touches laser trap play laser sound and kill player
        if (collision.gameObject.layer == LayerMask.NameToLayer("Laser"))
        {
            PlayerStates player = gameObject.GetComponent<PlayerStates>();

            rb.velocity = new Vector2(rb.velocity.x, jumpingPower / 2.5f);
            AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Laser);
            player.Death();
        }
    }
    private void WallSlide()
    {
        rb.velocity = new Vector2(0, 0);
        rb.gravityScale = 0;
    }
}
