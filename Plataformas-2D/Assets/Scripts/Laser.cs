using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private float timeUntilSpawn;
    public float startTimeUntilSpawn;

    [SerializeField] private GameObject laser;
    [SerializeField] private Transform whereToSpawn;

    void Update()
    {
        if (timeUntilSpawn <= 0)
        {
            Instantiate(laser, whereToSpawn.position, whereToSpawn.rotation);

            timeUntilSpawn = startTimeUntilSpawn;
        }
        else
        {
            timeUntilSpawn -= Time.deltaTime;
        }
    }
}
