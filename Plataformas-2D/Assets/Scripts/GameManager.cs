using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int world;
    public int stage;
    public float time;
    public bool isTimeRunning;

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        time = 0;
        isTimeRunning = true;
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    private void Update()
    {
        if (isTimeRunning)
        {
            Timer();
        }
    }

    //Start game
    public void NewGame()
    {
        time = 0;
        isTimeRunning = true;
        Time.timeScale = 1;
        AudioManager.Instance.PlayLevel1Music();

        LoadLevel1();
    }

    //Load specified scene
    private void LoadLevel1()
    {
        time = 0;
        isTimeRunning = true;
        Time.timeScale = 1;
        AudioManager.Instance.PlayLevel1Music();
        SceneManager.LoadScene("Level-1");
    }
    private void LoadLevel2()
    {
        time = 0;
        isTimeRunning = true;
        Time.timeScale = 1;
        AudioManager.Instance.PlayLevel2Music();
        SceneManager.LoadScene("Level-2");
    }




    //Call ResetLevel Function with the specified delay in parameters
    public void ResetLevel(float delay)
    {
        Invoke(nameof(ResetLevel), delay);
    }

    //Restart level and downgrade lives by 1, if there is no lives left loads GameOver Scene.
    public void ResetLevel()
    {
        Time.timeScale = 1;

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Level-1" || scene.name == "Level-1-Completed" || scene.name == "MainMenu")
        {
            LoadLevel1();
        }
        else
        {
            LoadLevel2();
        }

            
    }
    //Player completed the level 1
    public void CompleteLevel1()
    {
        SceneManager.LoadScene("Level-1-Completed");
        AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Victory);
    }
    //Player completed the level 2
    public void CompleteLevel2()
    {
        SceneManager.LoadScene("Level-2-Completed");
        AudioManager.Instance.PlaySoundEffect(AudioManager.SoundEffects.Victory);
    }
    //Increment timer every second
    public void Timer()
    {
            time += Time.deltaTime;
    }
}
