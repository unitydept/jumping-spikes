using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum SoundEffects
    {
        Jump,
        Laser,
        Spikes,
        Fall,
        Victory
    }
    public static AudioManager Instance;

    [SerializeField] private AudioClip jumpSound;
    [SerializeField] private AudioClip laserSound;
    [SerializeField] private AudioClip spikeSound;
    [SerializeField] private AudioClip FallSound;
    [SerializeField] private AudioClip victorySound;

    [SerializeField] private AudioClip menuMusic;
    [SerializeField] private AudioClip level1Music;
    [SerializeField] private AudioClip level2Music;

    [SerializeField] private AudioSource audioSourceSound;
    [SerializeField] private AudioSource audioSourceMusic;



    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    private void Start()
    {
        PlayMenuMusic();
    }

    public void PlaySoundEffect(SoundEffects soundEffects)
    {
        switch (soundEffects)
        {
            case SoundEffects.Jump:
                audioSourceSound.clip = jumpSound;
                break;
            case SoundEffects.Laser:
                audioSourceSound.clip = laserSound;
                break;
            case SoundEffects.Spikes:
                audioSourceSound.clip = spikeSound;
                break;
            case SoundEffects.Fall:
                audioSourceSound.clip = FallSound;
                break;
            case SoundEffects.Victory:
                audioSourceSound.clip = victorySound;
                break;
            default:
                break;
        }

        audioSourceSound.Play();
    }

    public void PlayMenuMusic()
    {
        audioSourceMusic.Stop();
        audioSourceMusic.clip = menuMusic;
        audioSourceMusic.Play();
    }
    public void PlayLevel1Music()
    {
        audioSourceMusic.Stop();
        audioSourceMusic.clip = level1Music;
        audioSourceMusic.Play();
    }
    public void PlayLevel2Music()
    {
        audioSourceMusic.Stop();
        audioSourceMusic.clip = level2Music;
        audioSourceMusic.Play();
    }

    public void PauseMusic()
    {
        audioSourceMusic.Pause();
    }

    public void ResumeMusic()
    {
        audioSourceMusic.Play();
    }
}
