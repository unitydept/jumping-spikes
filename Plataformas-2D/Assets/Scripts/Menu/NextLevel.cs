using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public void LoadNextLevel()
    {
        GameManager.Instance.time = 0;
        GameManager.Instance.isTimeRunning = true;
        AudioManager.Instance.PlayLevel2Music();
        SceneManager.LoadScene("Level-2");
    }
}
