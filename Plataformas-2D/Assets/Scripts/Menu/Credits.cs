using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    private void Start()
    {
        Invoke("WaitToEnd", 11f);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void WaitToEnd()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayCredits()
    {
        SceneManager.LoadScene("Credits");
    }
}
