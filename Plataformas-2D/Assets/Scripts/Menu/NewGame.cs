using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1.0f;
    }
    public void StartNewGame()
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.NewGame();
        }
        else
        {
            SceneManager.LoadScene("Level-1");
            AudioManager.Instance.PlayLevel1Music();
            Time.timeScale = 1;
        }
    }
}
