using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject PauseMenu;

    public void LoadMainMenu()
    {
        AudioManager.Instance.PlayMenuMusic();
        SceneManager.LoadScene("MainMenu");
    }

    public void RestartGame()
    {
        GameManager.Instance.NewGame();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            PauseMenu.SetActive(!PauseMenu.activeSelf);
            if (PauseMenu.activeSelf)
            {
                Time.timeScale = 0;
                AudioManager.Instance.PauseMusic();
            }
            else
            {
                Time.timeScale = 1;
                AudioManager.Instance.ResumeMusic();
            }
        }
    }
}
