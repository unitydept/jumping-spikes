# Jumping Spikes
## Descripción

Este es un juego de plataformas que se centra en superar los niveles evitando las trampas que se encuentran a lo largo del nivel.

El juego se compone de mecánicas de movimiento básicas, en las que se encuentran la tecla A para desplazarse a la izquierda y la tecla D para el desplazamiento en la dirección opuesta, es decir, hacia la derecha. El jugador también dispone de una mecánica de salto con la que podremos tanto saltar a través de trampas o saltar a las paredes para agarrarnos a ellas. Como añadido, también podremos saltar desde las paredes para escalar y progresar a lo largo del nivel.

El objetivo del juego es completar el nivel en la menor cantidad de tiempo posible. Debido a este motivo, el jugador tendrá que repetir múltiples veces los niveles para optimizar la ruta más veloz.

## Escenas del juego

El juego se compone de 7 escenas que se definen a continuación:

1. Tittle - En esta escena nos encontraremos los controles de movimiento del jugador y el objetivo principal del juego. Desde aquí podremos acceder al menú de juego pulsando la tecla "Enter" tal como aparece en pantalla.

2. Main Menu - Esta escena se compone simplemente de 3 botones. El primer botón (New Game) es para inicializar el juego cargando el primer nivel, el siguiente botón (Credits) nos lleva a la escena de créditos, donde se irán mostrando  y el segundo botón (Exit) para salir de la aplicación.

3. Credits - En esta escena se muestran los diferentes departamentos de desarrollo del juego con las respectivas personas que han aportado algo al proyecto.

4. Level-1 - En esta escena se desenlaza el primer nivel del juego. Aquí nos encontramos con el "Player" o jugador que controla el usuario, el cual tendrá que desplazarse por la escena hasta llegar el final de la misma. También se compone de 2 tilemaps, uno para el suelo con collider por el que avanza el jugador y otro para las trampas de pinchos con sus respectivos colliders. Ambos tilemaps hacen uso del elemento "Composite" para cargar solamente un collider gigante en vez de cargar uno por uno los colliders de todos los elementos que componen el tilemap. Al final del nivel nos encontraremos una puerta que tendremos que tocar para superar el nivel.

5. Level-2 - Aquí se desarrolla el segundo nivel del juego. Este nivel se compone de los mismos elementos que el primer nivel, pero con un añadido. Este elemento adicional que compone esta escena son las trampas láser. Este tipo de trampas se desactivan y activan después de un corto periodo de tiempo, haciendo que el jugador tenga que calcular el momento preciso para pasar a través de ellos.

6. Level-1-Completed - En esta pantalla se indica la victoria del jugador al conseguir completar el nivel 1 y se nos mostrará el total de tiempo invertido en ese nivel. También hay 2 botones, uno de ellos para volver al menú principal y el otro para avanzar al siguiente nivel.

7. Level-2-Completed - En esta escena se le indica al jugador que ha conseguido superar el nivel 2 y se le mostrará el tiempo invertido en este nivel. También encontraremos 2 botones, uno para acceder al menú principal y el otro para reiniciar el nivel.

## Estructura de código

**PlayerController ->** Este script se encarga de darle movilidad al jugador, permitiendo su desplazamiento horizontal y vertical contando el salto. También se encarga de cambiar las animaciones del jugador al ejecutar cada una de estas acciones mediante las variables en el componente "Animator" del jugador. Desde este script también cambiaremos la dirección del sprite cada vez que cambiemos de sentido y haremos dar un pequeño salto al jugador para evadir las trampas que se encuentran durante la progresión del nivel. Para permitir al jugador realizar el salto se comprueba si no está en el aire con un "CapsuleCast" que se sitúa en la parte inferior del personaje. En el caso de que el jugador esté en contacto con el suelo, el jugador podrá impulsarse hacia arriba con el salto.

**AudioManager ->** En este script se almacenan todos los archivos de audio para que puedan ser llamados desde cualquier otra clase. Esto permite activar los efectos de sonido al realizar una acción y activar la música ya sea del menú o del juego cuando sea necesario.

**GameManager ->** Aquí se lleva a cabo el control global del juego. En este script se lleva a cabo el cálculo del tiempo invertido en el nivel actual. El objeto que contiene este script no se destruye para poder mantener el recuento de los valores aunque se cambie de escena. También se encarga de cambiar a las escenas correspondientes si el jugador ha superado el nivel.

**GameView ->** Se encarga de actualizar los valores de la UI con los valores almacenados en el "GameManager" en tiempo real a medida que el jugador avanza durante la partida.

**DeathAnimation ->** En este script se desactivan los colliders del jugador y se aplica la animación de muerte de este, haciendo que caiga bajo el mapa atravesando los elementos en su camino.

**SideScrolling ->** Maneja el movimiento de la cámara para que siga al jugador en todo momento durante la escena "Level-1" y "Level-2".

**FinalSequence ->** Realiza la animación final cuando el jugador termina el nivel y cambia de escena a "Level-1-Completed" y "Level-2-Completed".

**DeathFall ->** Detecta si el jugador se ha caído del mapa y, en caso de que esto suceda, lo mata y reinicia el nivel actual.

**WallCheck ->** Comprueba si el jugador está próximo a una pared para agarrarse a ella. En caso alejarse de está también se encarga de que el jugador se suelte.

**Laser ->** Este script se utiliza en las trampas de láser que se encuentran en el segundo nivel y se encarga de instanciar un láser entre las dos plataformas láser.

## Recursos de terceros
### VFX

WinSound -> https://opengameart.org/content/oldschool-win-and-die-jump-and-run-sounds  
LoseSound -> https://opengameart.org/content/oldschool-win-and-die-jump-and-run-sounds  
JumpSound -> https://pixabay.com/sound-effects/sfx-jump-07-80241/  
FallSound -> https://mixkit.co/free-sound-effects/falling/  
SpikesSound -> https://pixabay.com/sound-effects/slashkut-108175/  
LaserSound -> https://opengameart.org/content/63-digital-sound-effects-lasers-phasers-space-etc  

MenuMusic -> https://opengameart.org/content/energetic-platformer-music-drop-table-bass  
Level-1-Music -> https://opengameart.org/content/technological-messup  
Level-2-Music -> https://opengameart.org/content/dynamosphere


### Sprites y Animaciones

**Player** - Warrior Free Asset -> https://assetstore.unity.com/packages/2d/characters/warrior-free-asset-195707  

El resto de Sprites utilizados en este proyecto han sido desarrollados por mi.

## Demo
Video: https://youtu.be/R9PZ2rNH8KY  
Itch.io: https://jorge-diaz.itch.io/jumping-spikes